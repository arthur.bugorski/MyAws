#!/usr/bin/env pwsh

param(
	[string] $filePath,
	[string] $cityName,
	[boolean] $isPreChange,
	[string] $server = 'http://localhost:8080'
)

[hashtable] $areaIdByCityName = @{
	'Montrea'       =   1 ;
	'Toronto'       =   2 ;
	'Boston'        =  80 ;
	'Philadelphia'  =  86 ;
	'Austin'        = 100 ;
	'Washington'    = 832 ;
};
[int] $areaId = $areaIdByCityName[ $cityName ];
if ( -not $areaId ) {
	Write-Error "No area id found for city: $cityName";
	return -1;
}

if (  -Not  ( Test-Path -Path $filePath -PathType leaf )  ) {
	Write-Error "Cannot find file with path '$filePath' ";
	return -2;
}


[string] $testExpectationProperty = If ( $isPreChange ) { 'before' } Else { 'after' };



# TODO for some reason this don't work with -Parallel
Select-Xml -Path $filePath -Namespace @{ x = '' } -XPath /xml/testSet | ForEach-Object { 
	$testSet = $_.Node;

	$testSet.Location | ForEach-Object {
		$location = $_;


		[hashtable] $test = @{
			before    = ( $testSet.inBefore -eq 'true' )          ;
			after     = ( $testSet.inAfter  -eq 'true' )          ;
			address   = "$($location.address), $($location.city)" ;
			latitude  = $location.coordinate.latitude             ;
			longitude = $location.coordinate.longitude            ;
		};

		[boolean] $locationIsInZone = ( Invoke-RestMethod   `
			-Method Get                                     `
			-Uri "$server/V7/api/projects/zone" `
			-Body @{ 
				areaId = $areaId;
				lat = $test.latitude; 
				lon = $test.longitude;
			} 
			).valid
		;

		
		
		[boolean] $expectation = $test[$testExpectationProperty];
		[string] $message = "$($test.address) ( $($test.latitude), $($test.longitude) )`nexpected: $expectation`tvalid: $locationIsInZone`n"; 
		[boolean] $testPassed = $locationIsInZone -eq $expectation;

		If ( $testPassed ) {
			Write-Information $message;
		} else {
			Write-Error $message;
		}
	}
}


