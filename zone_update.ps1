 #! /usr/bin/env pwsh

#
# This is a PowerShell script that runs on the open source PowerShell version 7 and higher.
# https://github.com/PowerShell/PowerShell
#       OS X: brew install powershell
#       Windows: choco install powershell-core
#       Ubuntu: https://docs.microsoft.com/en-us/powershell/scripting/install/install-ubuntu?view=powershell-7.2
# 

<# .SYNOPSIS #>
param( 
	[string] $cityName,     ## The city to update.
	[string] $kmlFilePath,  ## The KML file containing the folders of coordinates

	[Parameter(Mandatory=$true)]
	[string] $folderName    ## The name of the KML "folder" (aka "layer") containing the new coordinates.
)

Write-Debug "cityName: '$cityName'`nkmlFilePath: '$kmlFilePath'`nfolderName: '$folderName'"

if ( -Not $cityName ) {
	Write-Error "Need to provide the name of the city"
	return -1;
}



# validate that the input file
if (  -Not  ( Test-Path -Path $kmlFilePath -PathType leaf )  ) {
	Write-Error "Cannot find file with path '$kmlFilePath' ";
	return -2;
}

$kmlFileFullPath = ( Get-ChildItem -Path $kmlFilePath ).FullName;
Write-Debug "Using '$kmlFileFullPath' as coordinate source"


# TODO validate XML :: https://stackoverflow.com/questions/14423861/how-to-validate-xml-for-correct-syntax-format


# validate $folderName has a valid value
[string[]] $validFolderNames = @(); 
(
	Select-Xml                                                 `
		-Path ../Downloads/RND-14128_DMV_Service_Area.kml      `
		-Namespace @{ kml = 'http://www.opengis.net/kml/2.2' } `
		-XPath "/kml:kml/kml:Document/kml:Folder[kml:name]"    `
) | ForEach-Object { $validFolderNames += $_.Node.name };
if ( -Not ( $validFolderNames -Contains $folderName ) ) {
	Write-Error "'$folderName' is not the name of a valid folder in $kmlFileFullPath";
	Write-Error ("Valid folders are: `n" + ( $validFolderNames -join "`n" ) );
	return -3;
} else {
	Write-Debug "Reading from folder '$folderName'";
}




# 
# Nb: that if you wish to use this as raw SQL you need to convert to change the `$$ to just $$
# because the backtick ` is just for escaping the $ as $$ is a spectial variable in Powershell
# 

@"
/* Generated from '$kmlFilePath' using layer '$folderName' on $(date) */
DO LANGUAGE plpgsql `$$
	DECLARE
		city_name constant varchar := '$cityName';

		rp_area_to_update_id bigint;
		rp_old_area_count int;
		rp_new_area_count int;

		core_area_to_update_id bigint;
		core_old_area_count int;
		core_new_area_count int;
	BEGIN
			RAISE INFO '"%" is the city to be updated', city_name;

			/* create temp table to store values only once */
			DROP TABLE IF EXISTS new_coordinates;
			CREATE TEMPORARY TABLE
					new_coordinates (
						 sequence  SERIAL PRIMARY KEY
						,latitude  numeric(11,8) CHECK ( latitude  > 0 )
						,longitude numeric(11,8) CHECK ( longitude < 0 )
					)
			;
			INSERT INTO
					/*
					 * Reversed because they are in the KML reversed, and keeping them reversed
					 * keeps things easier to visually inspect.
					 */
					new_coordinates (
						 longitude
						,latitude
					)
			VALUES
				/*
				 * Longitude is East <-> West
				 * Latitude is North <-> South
				 * 
				 * Sanity check, in North America:
				 *       "longitude" is NEGATIVE
				 *       "latitude"  is POSITIVE
				 *
				 * https://s3.us-east-2.amazonaws.com/journeynorth.org/images/graphics/mclass/Lat_Long.gif
				 */
/*********************************** vvv New Coordinates Below vvv *************************************/
$(                                                                                                      `
	(                                                                                                   `
		Select-Xml                                                                                      `
			-Path $kmlFileFullPath                                                                      `
			-Namespace @{ kml = 'http://www.opengis.net/kml/2.2' }                                      `
			-XPath "/kml:kml/kml:Document/kml:Folder[kml:name='${folderName}']//kml:coordinates/text()" `
	).Node.Data                                                                                         `
	| ForEach-Object { $_ -Split ',0' }                                                                 `
	| ForEach-Object { $_.trim() }                                                                      `
	| Where-Object { $_ }                                                                               `
	| ForEach-Object {                                                                                  `
			<#
			 # In the KML the order is longitude then latitude, we have latitude and longitude
			 # so we have to reverse it.
			 #>
			( $long, $lat ) = $_.split(','); 
			$lat  = $lat.  PadRight(11,'0'); 
			$long = $long. PadRight(11,'0'); 
			"`t`t`t`t( ${long}, ${lat} )";
		}                                                                                               `
	| Join-String -Separator ",`n"
)
/*********************************** ^^^ New Coordinates Above ^^^ *************************************/
			;

			/*******************************************************************
			 *
			 * UPDATE RENORUN_PROD
			 *
			 ******************************************************************/
			SELECT
					id
			INTO
					rp_area_to_update_id
			FROM
					renorun_prod.areas
			WHERE
					name = city_name
				AND
					lang = 'EN'
			;
			ASSERT rp_area_to_update_id IS NOT NULL, 'Did not find id for city in renorun_prod.areas';
			RAISE LOG '% is the RenoRunProd area id', rp_area_to_update_id;

			SELECT
					COUNT(*)
			INTO
					rp_old_area_count
			FROM
					renorun_prod.area_coordinates
			WHERE
					area_id = rp_area_to_update_id
			;
			RAISE DEBUG 'there were % RP coordinates', rp_old_area_count;

			DELETE FROM
					renorun_prod.area_coordinates
			WHERE
					area_id = rp_area_to_update_id
			;
			INSERT INTO
					renorun_prod.area_coordinates (
						 area_id
						,latitude
						,longitude
						,sequence
					)
			SELECT
					 rp_area_to_update_id AS area_id
					,latitude
					,longitude
					,sequence
			FROM
					new_coordinates
			;
			SELECT
					COUNT(*)
			INTO
					rp_new_area_count
			FROM
					renorun_prod.area_coordinates
			WHERE
					area_id = rp_area_to_update_id
			;
			RAISE DEBUG 'there are now % RP coordinates', rp_new_area_count;



			/*******************************************************************
			 *
			 * UPDATE CORE
			 *
			 ******************************************************************/
			SELECT
					area_id
			INTO
					core_area_to_update_id
			FROM
					core.areas
			WHERE
					name = city_name
			;
			ASSERT core_area_to_update_id IS NOT NULL, 'Did not find id for city in core.areas';
			RAISE LOG '% is the core area id', core_area_to_update_id;

			SELECT
					COUNT(*)
			INTO
					core_old_area_count
			FROM
					core.area_coordinates
			WHERE
					area_id = core_area_to_update_id
			;
			RAISE DEBUG 'there were % core coordinates', core_old_area_count;

			DELETE FROM
					core.area_coordinates
			WHERE
					area_id = core_area_to_update_id
			;
			INSERT INTO
					core.area_coordinates (
						 area_id
						,latitude
						,longitude
						,sequence
					)
			SELECT
					 core_area_to_update_id AS area_id
					,latitude
					,longitude
					,sequence
			FROM
					new_coordinates
			;
			SELECT
					COUNT(*)
			INTO
					core_new_area_count
			FROM
					core.area_coordinates
			WHERE
					area_id = core_area_to_update_id
			;
			RAISE DEBUG 'there now are % core coordinates', core_new_area_count;

			DROP TABLE IF EXISTS new_coordinates;

			RAISE DEBUG 'Done!';
	END `$$;
"@
