Function Get-AwsSessionCredentials{
	[CmdletBinding()]
	[OutputType([String])]
	param(
			[String] $Name = $env:AWS_USER,
			[String] $MfaCode
	)

 	[Boolean] $haveValid2faFormat = $false; 
 	do{
 		$haveValid2faFormat = $MfaCode -match '^\d{6}$' ;
	 	if( -not $haveValid2faFormat ){
 			Write-Error "The 2FA should be a 6 digit number, not '$MfaCode'"
			$MfaCode = Read-Host -Prompt "Enter 2FA Code: ";
 		}	
 	}while( -not  $haveValid2faFormat );
 	Write-Debug "MFA Code: >$MfaCode<"

# exit code 253 seems to be cannot find credentials "Unable to locate credentials. You can configure credentials by running "aws configure".
# exit code 254 seems to be invalid credentials "An error occurred (InvalidClientTokenId) when calling the GetCallerIdentity operation: The security token included in the request is invalid."

	[String[]] $responseLines = aws sts get-session-token --serial-number "arn:aws:iam::805395716309:mfa/${Name}" --token-code "${MfaCode}" ;

	$awsExitCode = $LastExitCode ;

	if( -not ( $awsExitCode -eq 0 ) ){
		exit $awsExitCode ;
	}

	[String] $responseString = [system.String]::Join( "", $responseLines ) ;
	Write-Debug $responseString ;
	[System.Object] $responseJson = ConvertFrom-Json -InputObject $responseString ;

	return $responseJson.Credentials ;
}


Function Update-AwsSessionCredentials{
	[CmdletBinding()]
	param( 
		[String] $Name = $env:AWS_USER,
		[String] $MfaCode
	)

	$sessionCredentials = Get-AwsSessionCredentials -Name $Name -MfaCode $MfaCode ;
	Write-Debug "Got credentials: ${sessionCredentials}" ;

	[String] $awsAccessKeyId = $sessionCredentials.AccessKeyId ;
	[String] $awsSecretAccessKey = $sessionCredentials.SecretAccessKey ;
	[String] $awsSessionToken = $sessionCredentials.SessionToken ;

	[String] $newCredentials =
@"
[default]
aws_access_key_id = $awsAccessKeyId
aws_secret_access_key = $awsSecretAccessKey
aws_session_token = $awsSessionToken
"@;
	Write-Debug "New credentials will look like:" ;
	Write-Debug $newCredentials ;
	Remove-AwsCredentials ;

	Write-Output $newCredentials > "$home\.aws\credentials" ;
	Write-Debug "Credentials written!";
}


Function Remove-AwsCredentials{
	[String] $awsCredentialsPath = "$home/.aws/credentials" ;

	if( Test-Path -Path $awsCredentialsPath -PathType Leaf ){
		Write-Debug "removing $awsCredentialsPath" ;
		Remove-Item -Path $awsCredentialsPath ;

	}else{
		Write-Debug "No credentials found at: $awsCredentialsPath" ;

	}
}


Function Reset-AwsBaseCredentials{
	[CmdletBinding()]
	param(
			[String] $baseCredentialsPath = "$home/Documents/Powershell/Modules/MyAws/aws_base_credentials.txt" 
	)

	if( Test-Path -Path $baseCredentialsPath -PathType Leaf ){
		Remove-AwsCredentials ;
		Write-Debug "Using base credentials from $baseCredentialsPath" ;
		Get-Content $baseCredentialsPath | aws configure | Out-Null ;
	}else{
		Write-Error "Cannot find base AWS credentials at $baseCredentialsPath" ;
	}
}


Function Reset-AwsSessionCredentials{
	[CmdletBinding()]
	[OutputType([Boolean])]
	param(
			[String] $Name = $env:AWS_USER,
			[String] $MfaCode
	)

	Reset-AwsBaseCredentials ;
	Update-AwsSessionCredentials -Name $Name -MfaCode $MfaCode ;
	[String] $awsWhoAmIResponse = aws sts 'get-caller-identity' ;
	[System.Object] $awsWhoAmI = ConvertFrom-Json $awsWhoAmIResponse ;
 	
 	[String] $awsWhoAmIArn = $awsWhoAmI.Arn ;
 	[String[]] $awsWhoAmIArnPath = $awsWhoAmIArn -split '/' ;
 	[String] $awsUserName = $awsWhoAmIArnPath[ $awsWhoAmIArnPath.length - 1 ] ;
	
	return $awsUserName -eq $Name;
}

